FROM openjdk:11.0.8-jre-slim
EXPOSE 8080

COPY build/distributions/auctus-dashboard.tar /opt
WORKDIR /opt
RUN tar -xvf auctus-dashboard.tar

ENTRYPOINT ["/opt/auctus-dashboard/bin/auctus-dashboard"]
