import kotlinx.serialization.Serializable

@Serializable
data class TransactionInfo(
    val overallCount: Int,
    val lastMonthCount: Int,
    val lastWeekCount: Int,
    val lastDayCount: Int,
    val lastUpdated: String
) {
    companion object {
        const val path = "/backend/transactionInfo"
    }
}