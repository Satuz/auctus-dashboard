import kotlinx.serialization.Serializable

@Serializable
data class Pool(val id: String, val timestamp: String) {
    companion object {
        const val path = "/pools"
    }
}