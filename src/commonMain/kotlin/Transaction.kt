import kotlinx.serialization.Serializable

@Serializable
data class Transaction(val id: String, val timestamp: String) {
    companion object {
        const val path = "/backend/transactions"
    }
}