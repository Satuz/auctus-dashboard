import com.ccfraser.muirwik.components.MTypographyVariant
import com.ccfraser.muirwik.components.card.mCard
import com.ccfraser.muirwik.components.mTypography
import com.ccfraser.muirwik.components.table.*
import kotlinx.css.*
import react.RBuilder
import react.RComponent
import react.RState
import react.ReactElement
import styled.css
import styled.styledSpan

class TransactionComponent : RComponent<TransactionProps, RState>() {

    override fun RBuilder.render() {
        val lastUpdatedHeaderInfo = props.lastUpdated?.let { " last sync: ${props.lastUpdated}" } ?: ""

        mTypography(variant = MTypographyVariant.h6, gutterBottom = true) {
            +"Transactions"
            styledSpan {
                css {
                    color = Color("grey")
                    fontSize = 0.7.em
                }
                +lastUpdatedHeaderInfo
            }
        }

        mCard {
            css {
                marginBottom = 24.px
            }

            mTable {
                css {
                    tableLayout = TableLayout.fixed
                }

                mTableHead {
                    css {
                        color = Color.white
                        backgroundColor = Color("#3D3935")
                    }
                    mTableRow {
                        mTableCell(align = MTableCellAlign.left) {
                            css { color = Color.inherit }
                            +"Overall"
                        }
                        mTableCell {
                            css { color = Color.inherit }
                            +"Last month"
                        }
                        mTableCell {
                            css { color = Color.inherit }
                            +"Last week"
                        }
                        mTableCell(align = MTableCellAlign.right) {
                            css { color = Color.inherit }
                            +"Last day"
                        }
                    }
                }

                mTableBody {
                    mTableRow(key = "Transactions") {
                        val transactionInfo = props.transactionInfo
                        mTableCell(align = MTableCellAlign.left, className = "MuiTableCell-sizeSmall") {
                            +transactionInfo.overallCount.toString()

                        }
                        mTableCell(className = "MuiTableCell-sizeSmall") {
                            +transactionInfo.lastMonthCount.toString()

                        }
                        mTableCell(className = "MuiTableCell-sizeSmall") {
                            +transactionInfo.lastWeekCount.toString()

                        }
                        mTableCell(align = MTableCellAlign.right, className = "MuiTableCell-sizeSmall") {
                            +transactionInfo.lastDayCount.toString()

                        }
                    }

                }
            }
        }
    }
}

fun RBuilder.transactionComponent(handler: TransactionProps.() -> Unit): ReactElement {
    return child(TransactionComponent::class) {
        this.attrs(handler)
    }
}
