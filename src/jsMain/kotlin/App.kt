import com.ccfraser.muirwik.components.*
import com.ccfraser.muirwik.components.styles.ThemeOptions
import com.ccfraser.muirwik.components.styles.createMuiTheme
import io.ktor.client.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*
import io.ktor.http.*
import kotlinx.browser.window
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.css.*
import react.*
import react.dom.img
import styled.css
import styled.styledDiv

external interface AppState : RState {
    var transactionInfo: TransactionInfo
    var transactions: List<Transaction>
    var pools: List<Pool>

}

class App : RComponent<RProps, AppState>() {

    private var tabValue: Any = "All"

    override fun AppState.init() {
        transactions = listOf()
        transactionInfo = TransactionInfo(0, 0, 0, 0, "loading..")
        val mainScope = MainScope()
        mainScope.launch {
            val fetchedTransactionInfo = fetchInternTransactionInfo()
            setState {
                transactionInfo = fetchedTransactionInfo
                transactions = listOf()
                pools = listOf()
            }
        }
    }

    override fun RBuilder.render() {
        mCssBaseline()

        val themeOptions: ThemeOptions = js("({palette: { type: 'placeholder', primary: {main: 'placeholder'}}, overrides: {type: 'placeholder'}})")
        themeOptions.palette?.type = "light"
        themeOptions.palette?.primary.main = Color("#3D3935").toString()


        mThemeProvider(createMuiTheme(themeOptions)) {
            styledDiv {
                css {
                    backgroundColor = Color("#f7f7f7")
                }

                // Header
                mAppBar(color = MAppBarColor.primary, position = MAppBarPosition.static) {
                    mToolbar {
                        css {
                            flexWrap = FlexWrap.wrap
                        }
                        styledDiv {
                            css {
                                padding(6.px, 24.px, 12.px, 0.px)
                            }
                            img("Logo", "/images/auctus.png") {}
                        }
                        styledDiv {
                            mTypography("Auctus Dashboard", variant = MTypographyVariant.h6)
                        }
                        mTabs(tabValue, textColor = MTabTextColor.inherit, indicatorColor = MTabIndicatorColor.primary, onChange = { _, value -> setState { tabValue = value } }) {
                            mTab("All", "All") { css { minWidth = 70.px } }
                            mTab("Pools", "Pools") { css { minWidth = 70.px } }
                            mTab("Transactions", "Transactions") { css { minWidth = 70.px } }
                        }
                    }
                }

                // Content section
                mContainer {
                    css {
                        paddingTop = 20.px
                    }
                    when (tabValue) {
                        "All" -> all()
                        "Pools" -> tabPoolContainer()
                        "Transactions" -> tabTransactionContainer()
                    }
                }
            }
        }
    }

    private fun RBuilder.tabTransactionContainer() {
        transactionComponent {
            transactions = state.transactions
            transactionInfo = state.transactionInfo
            lastUpdated = state.transactionInfo.lastUpdated
        }
    }

    private fun RBuilder.tabPoolContainer() {
        poolComponent {
            pools = state.pools
            lastUpdated = "not updated"
        }
    }


    fun RBuilder.all() {
        tabPoolContainer()
        tabTransactionContainer()
    }
}

val jsonClient = HttpClient {
    install(JsonFeature) { serializer = KotlinxSerializer() }
    headersOf("Access-Control-Allow-Origin", "*")
}

val endpoint = window.location.origin

suspend fun fetchInternTransactions(): List<Transaction> {
    val transactions: List<Transaction> = jsonClient.get(endpoint + Transaction.path)
    return transactions
}

suspend fun fetchInternTransactionInfo(): TransactionInfo {
    val transactionInfo: TransactionInfo = jsonClient.get(endpoint + TransactionInfo.path)
    return transactionInfo
}

external interface TransactionProps : RProps {
    var transactionInfo: TransactionInfo
    var transactions: List<Transaction>
    var lastUpdated: String?
}

external interface PoolProps : RProps {
    var pools: List<Pool>
    var lastUpdated: String?
}

