import com.ccfraser.muirwik.components.MTypographyVariant
import com.ccfraser.muirwik.components.card.mCard
import com.ccfraser.muirwik.components.mTypography
import com.ccfraser.muirwik.components.table.*
import kotlinx.css.*
import react.RBuilder
import react.RComponent
import react.RState
import react.ReactElement
import styled.css
import styled.styledSpan

class PoolComponent : RComponent<PoolProps, RState>() {

    override fun RBuilder.render() {
        val lastUpdatedHeaderInfo = props.lastUpdated?.let { " last sync: ${props.lastUpdated}" } ?: ""

        mTypography(variant = MTypographyVariant.h6, gutterBottom = true) {
            +"Pools"
            styledSpan {
                css {
                    color = Color("grey")
                    fontSize = 0.7.em
                }
                +lastUpdatedHeaderInfo
            }
        }

        mCard {
            css {
                marginBottom = 24.px
            }

            mTable {
                css {
                    tableLayout = TableLayout.fixed
                }

                mTableHead {
                    css {
                        color = Color.white
                        backgroundColor = Color("#3D3935")
                    }
                    mTableRow {
                        mTableCell(align = MTableCellAlign.left) {
                            css { color = Color.inherit }
                            +"WBTC-USD PUT Options"
                        }
                        mTableCell {
                            css { color = Color.inherit }
                            +"WBTC-USD CALL Options"
                        }
                        mTableCell {
                            css { color = Color.inherit }
                            +"ETH-USD PUT Options"
                        }
                        mTableCell(align = MTableCellAlign.right) {
                            css { color = Color.inherit }
                            +"ETH-USD CALL Options"
                        }
                    }
                }

                mTableBody {
                    mTableRow(key = "Pools") {
                        val pools = props.pools
                        mTableCell(align = MTableCellAlign.left, className = "MuiTableCell-sizeSmall") {
                            +"0"

                        }
                        mTableCell(className = "MuiTableCell-sizeSmall") {
                            +"0"

                        }
                        mTableCell(className = "MuiTableCell-sizeSmall") {
                            +"0"

                        }
                        mTableCell(align = MTableCellAlign.right, className = "MuiTableCell-sizeSmall") {
                            +"0"

                        }
                    }

                }
            }
        }
    }
}

fun RBuilder.poolComponent(handler: PoolProps.() -> Unit): ReactElement {
    return child(PoolComponent::class) {
        this.attrs(handler)
    }
}
