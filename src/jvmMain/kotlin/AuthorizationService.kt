import io.ktor.client.*
import io.ktor.client.features.json.*
import io.ktor.client.request.*

class AuthorizationService {

    companion object {
        val metadataUrl = "http://metadata/computeMetadata/v1/instance/service-accounts/default/identity?audience="

        suspend fun getTokenForBackendService(serviceUrl: String): String {
            val httpClient = HttpClient {
                install(JsonFeature)
            }
            return httpClient.get("$metadataUrl$serviceUrl") {
                header("Metadata-Flavor", "Google")
            }
        }
    }
}

