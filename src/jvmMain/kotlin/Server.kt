import io.ktor.application.*
import io.ktor.client.*
import io.ktor.client.features.json.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.serialization.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.utils.io.*
import org.slf4j.event.Level

var backendURL = "https://auctus-backend-wkuor3breq-ez.a.run.app/"

fun Application.module() {
    val env = environment.config.propertyOrNull("ktor.environment")?.getString()
    when (env) {
        "dev" -> backendURL = "http://localhost:8081"
        "prod" -> backendURL = "https://auctus-backend-wkuor3breq-ez.a.run.app"
    }
}

fun main() {
    embeddedServer(Netty, 8080) {
        install(ContentNegotiation) {
            json()
        }
        install(CallLogging) {
            level = Level.TRACE
        }
        install(CORS) {
            method(HttpMethod.Get)
            method(HttpMethod.Post)
            method(HttpMethod.Delete)
            anyHost()
        }
        install(Compression) {
            gzip()
        }
        routing {
            get("/") {
                call.respondText(
                    this::class.java.classLoader.getResource("index.html")!!.readText(),
                    ContentType.Text.Html
                )
            }
            static("/") {
                resources("")
            }
            route("/backend/*") {
                get {
                    val httpClient = HttpClient {
                        install(JsonFeature)
                    }

                    val proxyUrl = getProxyUrl(call.request.uri)
                    val token = AuthorizationService.getTokenForBackendService(backendURL)
                    val response = httpClient.request<HttpResponse>(proxyUrl) {
                        header(HttpHeaders.Authorization, "Bearer $token")
                    }
                    val proxiedHeaders = response.headers
                    val contentType = proxiedHeaders[HttpHeaders.ContentType]
                    val contentLength = proxiedHeaders[HttpHeaders.ContentLength]

                    call.application.environment.log.info("Backend Url: $backendURL with $token")

                    call.respond(object : OutgoingContent.WriteChannelContent() {
                        override val contentLength: Long? = contentLength?.toLong()
                        override val contentType: ContentType? = contentType?.let { ContentType.parse(it) }
                        override val status: HttpStatusCode? = response.status
                        override suspend fun writeTo(channel: ByteWriteChannel) {
                            response.content.copyAndClose(channel)
                        }
                    })
                }
            }
        }
    }.start(wait = true)
}

private fun getProxyUrl(uri: String): String {
    val routedUri = .replace("/backend", "")
    val urlString = "${backendURL}${routedUri}"
    return urlString
}

