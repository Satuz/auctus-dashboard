# Configure GCP project
provider "google" {
  project = "vaulted-acolyte-312816"
}

# Deploy image to Cloud Run
resource "google_cloud_run_service" "auctus-dashboard" {
  name = "auctus-dashboard"
  location = "europe-west4"
  template {
    spec {
      containers {
        image = "gcr.io/vaulted-acolyte-312816/auctus-dashboard"
      }
    }
  }
  traffic {
    percent = 100
    latest_revision = true
  }
}

# Create public access
data "google_iam_policy" "noauth" {
  binding {
    role = "roles/run.invoker"
    members = [
      "allUsers",
    ]
  }
}

# Enable public access on Cloud Run service
resource "google_cloud_run_service_iam_policy" "noauth" {
  location = google_cloud_run_service.auctus-dashboard.location
  project = google_cloud_run_service.auctus-dashboard.project
  service = google_cloud_run_service.auctus-dashboard.name
  policy_data = data.google_iam_policy.noauth.policy_data
}